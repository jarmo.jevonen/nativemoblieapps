package com.example.connect4

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.get
import kotlinx.android.synthetic.main.board.*
import kotlinx.android.synthetic.main.stats.*


class MainActivity : AppCompatActivity() {
    companion object {
        private val TAG = this::class.java.declaringClass!!.simpleName
    }
    private lateinit var controller: Controller


    var nextMoveByX = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            createANewGame(null)
        } else {
            createANewGame(savedInstanceState.getIntArray(C.BUNDLE_SAVE_STATE))
        }

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState")
        outState.putIntArray(C.BUNDLE_SAVE_STATE, controller.getGameSaveState())
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    private fun createANewGame(gameState: IntArray?) {
        gameBoard.removeAllViews()
        textViewStats.setText(R.string.statsPlayerTurnText)
        startNewGameButton.visibility = View.INVISIBLE
        var currentPlayerResource = R.drawable.gameboard_button_pressed_p1
        if (gameState != null) {
            Log.d(TAG, "current turn ${gameState[C.GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER]}")
            if (gameState[C.GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER] == C.PLAYER_2) {
                Log.d(TAG, "is player 2 - RED")
                currentPlayerResource = R.drawable.gameboard_button_pressed_p2
            }
        }
        statsPlayerTurnButton.setBackgroundResource(currentPlayerResource)
        Log.d(TAG, "set to: ${statsPlayerTurnButton.background}")
        (gameBoard.layoutParams as ConstraintLayout.LayoutParams)
            .dimensionRatio = "${C.BOARD_WIDTH.toString()}:${C.BOARD_HEIGHT.toString()}"
        for (y in 0.until(C.BOARD_HEIGHT)) {
            val tableRow = layoutInflater
                .inflate(R.layout.table_row, gameBoard, false) as TableRow
            gameBoard.addView(tableRow)
            for (x in 0.until(C.BOARD_WIDTH)) {
                layoutInflater.inflate(R.layout.button, tableRow)
                if (gameState != null) {
                    displayCellStateChange(y, x, gameState[y * C.BOARD_WIDTH + x])
                }
            }
        }
        controller = Controller(this, gameState)
    }

    fun onStartNewGameClicked(view: View) {
        createANewGame(null)
    }

    fun buttonOnClick(view: View) {
        val clickedTableRow = (view.parent as TableRow)
        val x = clickedTableRow.indexOfChild(view)
        controller.sendUserInputCoordinatesToGameEngine(x)
    }

    public fun markWinnerTiles(tiles: ArrayList<IntArray>, winner: Int) {
        playerWon()
        for (tile in tiles) {
            displayCellStateChange(tile[0], tile[1], winner)
        }
    }

    public fun displayPlayerTurn(player: Int) {
        if (player == C.PLAYER_1) {
            statsPlayerTurnButton.setBackgroundResource(R.drawable.gameboard_button_pressed_p1)
        }
        if (player == C.PLAYER_2) {
            statsPlayerTurnButton.setBackgroundResource(R.drawable.gameboard_button_pressed_p2)
        }
    }

    public fun displayCellStateChange(y: Int, x: Int, player: Int) {
        var playerColor = R.drawable.gameboard_button_default
        if (player == C.PLAYER_1) {
            playerColor = R.drawable.gameboard_button_pressed_p1
        }
        if (player == C.PLAYER_2) {
            playerColor = R.drawable.gameboard_button_pressed_p2
        }
        if (player == C.WINNER_PLAYER1) {
            playerColor = R.drawable.gameboard_button_pressed_winner_p1
        }
        if (player == C.WINNER_PLAYER2) {
            playerColor = R.drawable.gameboard_button_pressed_winner_p2
        }
        ((gameBoard[y] as TableRow)[x] as Button).setBackgroundResource(playerColor)
    }

    fun setGameStateDraw() {
        statsPlayerTurnButton.visibility = View.GONE
        textViewStats.setText(R.string.statsDrawText)
        startNewGameButton.visibility = View.VISIBLE
    }

    private fun playerWon() {
        textViewStats.setText(R.string.statsPlayerWonText)
        startNewGameButton.visibility = View.VISIBLE
    }
}