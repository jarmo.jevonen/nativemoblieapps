package com.example.connect4

class C {
    companion object {
        const val PLAYER_1 = 1
        const val PLAYER_2 = 2
        const val WINNER_PLAYER1 = 3
        const val WINNER_PLAYER2 = 4

        const val BOARD_HEIGHT = 6
        const val BOARD_WIDTH = 7

        const val WIN_CONDITION = 4

        const val GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER = BOARD_HEIGHT * BOARD_WIDTH
        const val GAME_STATE_ARRAY_INDEX_LAST_Y_COORDINATE = GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER + 1
        const val GAME_STATE_ARRAY_INDEX_LAST_X_COORDINATE = GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER + 2
        const val BUNDLE_SAVE_STATE = "BUNDLE_SAVE_STATE"
    }
}