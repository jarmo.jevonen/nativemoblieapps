package com.example.connect4

import android.util.Log

class GameEngine(private val controller: Controller, private val gameState: IntArray?) {
    public enum class CellState {
        EMPTY,
        PLAYER1,
        PLAYER2
    }
    private var moveByPlayer1 = true
    private var gameOver = false
    private var lastMoveYCoordinate = -1
    private var lastMoveXCoordinate = -1


    private var gameBoard = Array(C.BOARD_HEIGHT) {
        arrayOfNulls<CellState>(
            C.BOARD_WIDTH
        )
    }

    init {
        initializeGameBoard()
        if (gameState != null) {
            moveByPlayer1 = gameState[C.GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER] == CellState.PLAYER1.ordinal
            lastMoveYCoordinate = gameState[C.GAME_STATE_ARRAY_INDEX_LAST_Y_COORDINATE]
            lastMoveXCoordinate = gameState[C.GAME_STATE_ARRAY_INDEX_LAST_X_COORDINATE]
            hasPlayerWon()
            isDraw()
        }
    }



    private fun initializeGameBoard() {
        for (y in 0 until C.BOARD_HEIGHT) {
            for (x in 0 until C.BOARD_WIDTH){
                var cellState = CellState.EMPTY
                if (gameState != null) {
                    if (gameState[y * C.BOARD_WIDTH + x] == CellState.PLAYER1.ordinal) {
                        cellState = CellState.PLAYER1
                    } else if (gameState[y * C.BOARD_WIDTH + x] == CellState.PLAYER2.ordinal) {
                        cellState = CellState.PLAYER2
                    }
                }
                gameBoard[y][x] = cellState
            }
        }
    }

    fun calculatePlayerMove(x: Int) {
        if (!gameOver) {
            for (y in C.BOARD_HEIGHT - 1 downTo  0) {
                if (gameBoard[y][x]!! == CellState.EMPTY) {
                    val cellState = if (moveByPlayer1) CellState.PLAYER1 else CellState.PLAYER2
                    gameBoard[y][x] = cellState
                    controller.sendPlayerMoveToUi(y, x, cellState)
                    lastMoveYCoordinate = y
                    lastMoveXCoordinate = x
                    break
                }
            }
            if (!hasPlayerWon() && !isDraw()) {
                moveByPlayer1 = !moveByPlayer1
                controller.declarePlayerTurn(if (moveByPlayer1) CellState.PLAYER1.ordinal else CellState.PLAYER2.ordinal)
            }
        }
    }

    private fun isDraw(): Boolean {
        var hasEmptyCells = false
        for (y in 0 until  C.BOARD_HEIGHT) {
            for (x in 0 until C.BOARD_WIDTH) {
                if (gameBoard[y][x] == CellState.EMPTY) {
                    hasEmptyCells = true
                    break
                }
            }
        }
        if (!hasEmptyCells) {
            gameOver = true
            controller.declareDraw()
        }
        return !hasEmptyCells
    }

    private fun hasPlayerWon(): Boolean {
        val y = lastMoveYCoordinate
        val x = lastMoveXCoordinate
        val wonHorizontally = findAdjacentTilesCountHorizontally(y)
        val wonVertically = findAdjacentTilesCountVertically(x)
        val wonDiagonallyUp = findAdjacentTilesCountDiagonalUp(y, x)
        val wonDiagonallyDown = findAdjacentTilesCountDiagonalDown(y, x)
        val winner = if (moveByPlayer1) C.WINNER_PLAYER1 else C.WINNER_PLAYER2
        Log.d("won", wonDiagonallyUp.toString())
        Log.d("won", wonDiagonallyDown.toString())
        Log.d("won", wonHorizontally.toString())
        Log.d("won", wonVertically.toString())
        if (wonDiagonallyDown.isNotEmpty()) {
            controller.declareWinnerCombination(wonDiagonallyDown, winner)
            return true
        }
        if (wonDiagonallyUp.isNotEmpty()) {
            controller.declareWinnerCombination(wonDiagonallyUp, winner)
            return true
        }
        if (wonHorizontally.isNotEmpty()) {
            controller.declareWinnerCombination(wonHorizontally, winner)
            return true
        }
        if (wonVertically.isNotEmpty()) {
            controller.declareWinnerCombination(wonVertically, winner)
            return true
        }
        return false
    }

    private fun  findAdjacentTilesCountHorizontally(yCoordinate: Int)
            : java.util.ArrayList<IntArray> {
        val adjacentTilesList = ArrayList<ArrayList<IntArray>>()
        val adjacentPairsList = ArrayList<IntArray>()
        val currentPlayerTile = if (moveByPlayer1)
            CellState.PLAYER1 else CellState.PLAYER2
        for (x in 0 until C.BOARD_WIDTH) {
            if (gameBoard[yCoordinate][x] == currentPlayerTile) {
                adjacentPairsList.add(intArrayOf(yCoordinate, x))
            } else {
                adjacentTilesList.add(adjacentPairsList.toCollection(ArrayList()))
                adjacentPairsList.clear()
            }
        }
        if (adjacentPairsList.isNotEmpty()) {
            adjacentTilesList.add(adjacentPairsList)
        }
        return compareTilesToWinCondition(adjacentTilesList)
    }

    private fun  findAdjacentTilesCountVertically(xCoordinate: Int):
            java.util.ArrayList<IntArray> {
        val adjacentTilesList = ArrayList<ArrayList<IntArray>>()
        val adjacentPairsList = ArrayList<IntArray>()
        val currentPlayerTile = if (moveByPlayer1)
            CellState.PLAYER1 else CellState.PLAYER2
        for (y in 0 until C.BOARD_HEIGHT) {
            if (gameBoard[y][xCoordinate] == currentPlayerTile) {
                adjacentPairsList.add(intArrayOf(y, xCoordinate))
            } else {
                adjacentTilesList.add(adjacentPairsList.toCollection(ArrayList()))
                adjacentPairsList.clear()
            }
        }
        if (adjacentPairsList.isNotEmpty()) {
            adjacentTilesList.add(adjacentPairsList)
        }
        return compareTilesToWinCondition(adjacentTilesList)
    }

    private fun calculateDiagonallyUpStartingPoint(y: Int, x: Int): Pair<Int, Int> {
        var yCoordinate = C.BOARD_HEIGHT - 1
        var xCoordinate = x - (C.BOARD_HEIGHT - 1 - y)
        if (xCoordinate < 0) {
            yCoordinate = C.BOARD_HEIGHT - xCoordinate
            xCoordinate = 0
        }
        return Pair(yCoordinate, xCoordinate)
    }

    private fun calculateDiagonallyDownStartingPoint(y: Int, x: Int): Pair<Int, Int> {
        var yCoordinate = C.BOARD_HEIGHT - 1
        var xCoordinate = x + (C.BOARD_HEIGHT - 1 - y)
        if (xCoordinate >= C.BOARD_WIDTH) {
            yCoordinate = C.BOARD_WIDTH - xCoordinate
            xCoordinate = C.BOARD_WIDTH - 1
        }
        return Pair(yCoordinate, xCoordinate)
    }

    private fun  findAdjacentTilesCountDiagonalUp(yCoordinate: Int, xCoordinate: Int):
            java.util.ArrayList<IntArray> {
        val adjacentTilesList = ArrayList<ArrayList<IntArray>>()
        val adjacentPairsList = ArrayList<IntArray>()
        val currentPlayerTile = if (moveByPlayer1)
            CellState.PLAYER1 else CellState.PLAYER2
        val startingPoint = calculateDiagonallyUpStartingPoint(yCoordinate, xCoordinate)
        var xCo = startingPoint.second
        for (y in C.BOARD_HEIGHT - 1 downTo C.BOARD_HEIGHT - startingPoint.first - 1) {
            if (xCo >= C.BOARD_WIDTH || y < 0) {
                break
            }
             if (gameBoard[y][xCo] == currentPlayerTile) {
                adjacentPairsList.add(intArrayOf(y, xCo))
            } else {
                adjacentTilesList.add(adjacentPairsList.toCollection(ArrayList()))
                adjacentPairsList.clear()
            }
            xCo++
        }
        if (adjacentPairsList.isNotEmpty()) {
            adjacentTilesList.add(adjacentPairsList)
        }
        return compareTilesToWinCondition(adjacentTilesList)
    }

    private fun  findAdjacentTilesCountDiagonalDown(yCoordinate: Int, xCoordinate: Int):
            java.util.ArrayList<IntArray> {
        val adjacentTilesList = ArrayList<ArrayList<IntArray>>()
        val adjacentPairsList = ArrayList<IntArray>()
        val currentPlayerTile = if (moveByPlayer1)
            CellState.PLAYER1 else CellState.PLAYER2
        val startingPoint = calculateDiagonallyDownStartingPoint(yCoordinate, xCoordinate)
        var xCo = startingPoint.second
        for (y in C.BOARD_HEIGHT - 1 downTo C.BOARD_HEIGHT - startingPoint.first - 1) {
            if (xCo < 0) {
                break
            }
            if (gameBoard[y][xCo] == currentPlayerTile) {
                adjacentPairsList.add(intArrayOf(y, xCo))
            } else {
                adjacentTilesList.add(adjacentPairsList.toCollection(ArrayList()))
                adjacentPairsList.clear()
            }
            xCo--
        }
        if (adjacentPairsList.isNotEmpty()) {
            adjacentTilesList.add(adjacentPairsList)
        }
        return compareTilesToWinCondition(adjacentTilesList)
    }

    private fun compareTilesToWinCondition(
        adjacentTilesList: ArrayList<ArrayList<IntArray>>)
            : java.util.ArrayList<IntArray> {
        for (adjacentTiles in adjacentTilesList) {
            if (adjacentTiles.count() >= C.WIN_CONDITION) {
                gameOver = true
                return adjacentTiles;
            }
        }
        return ArrayList<IntArray>()
    }

    fun getCurrentState(): IntArray {
        val gameState = IntArray(C.BOARD_WIDTH * C.BOARD_HEIGHT + 3)
        for (y in 0 until C.BOARD_HEIGHT) {
            for (x in 0 until C.BOARD_WIDTH) {
                gameState[C.BOARD_WIDTH * y + x] = gameBoard[y][x]!!.ordinal
            }
        }
        gameState[C.GAME_STATE_ARRAY_INDEX_CURRENT_PLAYER] =  if (moveByPlayer1) CellState.PLAYER1.ordinal
        else CellState.PLAYER2.ordinal
        gameState[C.GAME_STATE_ARRAY_INDEX_LAST_Y_COORDINATE] = lastMoveYCoordinate
        gameState[C.GAME_STATE_ARRAY_INDEX_LAST_X_COORDINATE] = lastMoveXCoordinate
        return gameState
    }


}