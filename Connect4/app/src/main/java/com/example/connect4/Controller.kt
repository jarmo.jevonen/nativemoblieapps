package com.example.connect4

class Controller (private val mainActivity: MainActivity, gameState: IntArray?) {
    private val gameEngine = GameEngine(this, gameState)


    fun declareWinnerCombination(winningTiles: ArrayList<IntArray>, winner: Int) {
        mainActivity.markWinnerTiles(winningTiles, winner)
    }

    fun declareDraw() {
        mainActivity.setGameStateDraw()
    }

    fun sendUserInputCoordinatesToGameEngine( x: Int) {
        gameEngine.calculatePlayerMove(x)
    }

    fun sendPlayerMoveToUi(y: Int, x: Int, player: GameEngine.CellState) {
        mainActivity.displayCellStateChange(y, x, player.ordinal)
    }

    fun getGameSaveState(): IntArray {
        return gameEngine.getCurrentState()
    }

    fun declarePlayerTurn(player: Int) {
        mainActivity.displayPlayerTurn(player)
    }
}