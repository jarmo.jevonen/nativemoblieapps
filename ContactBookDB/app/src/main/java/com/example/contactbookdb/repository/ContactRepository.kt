package com.example.contactbookdb.repository


import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.contactbookdb.DbHelper
import com.example.contactbookdb.model.Contact


class ContactRepository(val context: Context) {
    private lateinit var dbHelper: DbHelper
    private lateinit var db: SQLiteDatabase

    fun open(): ContactRepository {
        dbHelper = DbHelper(context)
        db = dbHelper.writableDatabase
        db.execSQL("PRAGMA foreign_keys=ON")
        return this
    }

    fun close() {
        dbHelper.close()
    }

    fun update(contact: Contact) {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.CONTACT_ID, contact.id)
        contentValues.put(DbHelper.CONTACT_PERSON_ID, contact.contactPersonId)
        contentValues.put(DbHelper.CONTACT_CONTACT_TYPE_ID, contact.contactContactTypeId)
        contentValues.put(DbHelper.CONTACT_CONTACT_NAME, contact.contactContactName)
        db.update(DbHelper.CONTACTS_TABLE_NAME, contentValues, "${DbHelper.CONTACT_ID} = ?", arrayOf(contact.id.toString()))
    }

    fun delete(contact: Contact) {
        db.delete(DbHelper.CONTACTS_TABLE_NAME, "${DbHelper.CONTACT_ID} = ?", arrayOf(contact.id.toString()))
    }

    fun add(contact : Contact): Long {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.CONTACT_PERSON_ID, contact.contactPersonId)
        contentValues.put(DbHelper.CONTACT_CONTACT_TYPE_ID, contact.contactContactTypeId)
        contentValues.put(DbHelper.CONTACT_CONTACT_NAME, contact.contactContactName)
        contact.id = db.insert(DbHelper.CONTACTS_TABLE_NAME, null, contentValues)
        return contact.id
    }

    fun getAll(): MutableList<Contact> {
        val contacts = ArrayList<Contact>()

        val columns = arrayOf(
            DbHelper.CONTACT_ID,
            DbHelper.CONTACT_PERSON_ID,
            DbHelper.CONTACT_CONTACT_TYPE_ID,
            DbHelper.CONTACT_CONTACT_NAME
        )

        val cursor = db.query(DbHelper.CONTACTS_TABLE_NAME, columns, null, null, null, null, null)

        while (cursor.moveToNext()) {
            contacts.add(
                Contact(
                    cursor.getLong(cursor.getColumnIndex(DbHelper.CONTACT_ID)),
                    cursor.getLong(cursor.getColumnIndex(DbHelper.CONTACT_PERSON_ID)),
                    cursor.getLong(cursor.getColumnIndex(DbHelper.CONTACT_CONTACT_TYPE_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.CONTACT_CONTACT_NAME))
                )
            )
        }
        cursor.close()
        return contacts
    }
}
