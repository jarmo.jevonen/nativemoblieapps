package com.example.contactbookdb.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import com.example.contactbookdb.R
import com.google.android.material.textfield.TextInputLayout
import java.lang.ClassCastException

class DialogPersonAdd : DialogFragment() {
    private lateinit var listener: IDialogPersonAddListener



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(this.context)
        val inflater = requireActivity().layoutInflater
        val inflatedView = inflater.inflate(R.layout.dialog_add_person, null)
        builder.setView(inflatedView)
            .setPositiveButton("Add") { dialogInterface, i ->
                val firstName =
                    inflatedView.findViewById<TextInputLayout>(R.id.dialog_add_person_text_input_layout_first_name).editText!!.text.toString()
                val lastName =
                    inflatedView.findViewById<TextInputLayout>(R.id.dialog_add_person_text_input_layout_last_name).editText!!.text.toString()
                listener.onDialogPositiveClick(this, firstName, lastName)
                Log.d("Foo", "pos")
            }
            .setNegativeButton("Cancel") { dialogInterface, i ->
                Log.d("Foo", "neg")
                dialog?.cancel()
            }
            .setTitle(R.string.dialog_person_add_title)
        return builder.create()
    }

    interface IDialogPersonAddListener {
        fun onDialogPositiveClick(dialog: DialogFragment, firstName: String, lastName: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IDialogPersonAddListener
        } catch (e: ClassCastException) {
            throw ClassCastException(("$context must implment ${this::class.java.simpleName}Listener"))
        }
    }
}