package com.example.contactbookdb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.example.contactbookdb.R
import com.example.contactbookdb.model.ContactType
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.recycler_view_contact_types_row.view.*

class RecyclerViewContactTypesAdapter(private val context: Context, private val contactTypes: MutableList<ContactType>) : RecyclerView.Adapter<RecyclerViewContactTypesAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private lateinit var clickListener: IContactTypeClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = inflater.inflate(R.layout.recycler_view_contact_types_row, parent, false)
        return ViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contactType = contactTypes.get(position)
        holder.textInputLayoutValue.editText!!.setText(contactType.contactTypeValue)
        holder.buttonEdit.setOnClickListener {
            contactType.contactTypeValue = holder.textInputLayoutValue.editText!!.text.toString()
            clickListener.onEditClicked(contactType)
        }
        holder.buttonDelete.setOnClickListener {
            clickListener.onDeleteClicked(contactType)
        }
    }

    override fun getItemCount(): Int {
        return contactTypes.count()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textInputLayoutValue: TextInputLayout = itemView.recycler_view_contact_types_row_text_input_layout_value
        val buttonEdit: Button = itemView.recycler_view_contact_types_row_button_edit
        val buttonDelete: Button = itemView.recycler_view_contact_types_row_button_delete
    }

    interface IContactTypeClickListener {
        fun onDeleteClicked(contactType: ContactType)
        fun onEditClicked(contactType: ContactType)
    }

    fun setOnClickListener(clickListener: IContactTypeClickListener) {
        this.clickListener = clickListener
    }
}
