package com.example.contactbookdb.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import com.example.contactbookdb.R
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputLayout
import java.lang.ClassCastException

class DialogContactAdd : DialogFragment() {
    companion object {
        const val PERSON_ID = "PersonId"
        const val CONTACT_TYPES_IDS = "ContactTypesIds"
        const val CONTACT_TYPES_VALUES = "ContactTypesValues"

        fun newInstance(personId: Long, contactTypesIds: LongArray, contactTypesValues: Array<String>) : DialogContactAdd {
            val args = Bundle()
            args.putLong(PERSON_ID, personId)
            args.putLongArray(CONTACT_TYPES_IDS, contactTypesIds)
            args.putStringArray(CONTACT_TYPES_VALUES, contactTypesValues)

            val dialog = DialogContactAdd()
            dialog.arguments = args
            return dialog
        }
    }
    private lateinit var listener: IDialogContactAddListener
    private var contactTypeId = 0L



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(this.context)
        val inflater = requireActivity().layoutInflater
        val inflatedView = inflater.inflate(R.layout.dialog_add_contact, null)
        val chipGroup = inflatedView.findViewById<ChipGroup>(R.id.dialog_add_contact_chip_group_type)

        val personId = arguments?.getLong(PERSON_ID)
        val contactTypesIds = arguments?.getLongArray(CONTACT_TYPES_IDS)
        val contactTypesValues = arguments?.getStringArray(CONTACT_TYPES_VALUES)

        if (contactTypesIds != null && contactTypesValues != null) {
            for (i in contactTypesIds.indices) {
                val chip = inflater.inflate(R.layout.chip, chipGroup, false) as Chip
                chip.text = contactTypesValues[i]
                chip.id = contactTypesIds[i].toInt()
                chip.tag = contactTypesIds[i].toInt()
                if (i == 0) {
                    chip.isChecked = true
                    contactTypeId = contactTypesIds[i]
                }
                chip.setOnCheckedChangeListener { compoundButton, isChecked ->
                    if (isChecked) {
                        contactTypeId = compoundButton.tag.toString().toLong()
                    }
                }
                chipGroup.addView(chip)
            }
        }


        builder.setView(inflatedView)
            .setPositiveButton("Add") { dialogInterface, i ->
                val name =
                    inflatedView.findViewById<TextInputLayout>(R.id.dialog_add_contact_text_input_layout_name).editText!!.text.toString()
                if (contactTypeId != 0L && personId != null && personId != 0L) {
                    listener.onDialogPositiveClick(this, personId, name, contactTypeId)
                } else {
                    Log.d("Chip", "ContactTypeId is still $contactTypeId")
                }
                Log.d("Foo", "pos")
            }
            .setNegativeButton("Cancel") { dialogInterface, i ->
                Log.d("Foo", "neg")
                dialog?.cancel()
            }
            .setTitle(R.string.dialog_contact_add_title)
        return builder.create()
    }

    interface IDialogContactAddListener {
        fun onDialogPositiveClick(dialog: DialogFragment, personId: Long, name: String, contactTypeId: Long)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IDialogContactAddListener
        } catch (e: ClassCastException) {
            throw ClassCastException(("$context must implment ${this::class.java.simpleName}Listener"))
        }
    }
}