package com.example.contactbookdb.model

class Person {
    var id: Long = 0
    var firstName: String = ""
    var lastName: String = ""
    var contacts: MutableList<Contact> = ArrayList()

    constructor(id : Long, firstName : String, lastName : String) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
    }

    constructor(firstName : String, lastName : String) : this(0L, firstName, lastName)

}