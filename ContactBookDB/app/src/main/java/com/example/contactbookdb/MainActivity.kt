package com.example.contactbookdb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.contactbookdb.adapter.RecyclerViewContactsAdapter
import com.example.contactbookdb.dialog.DialogPersonEdit
import com.example.contactbookdb.model.Contact
import com.example.contactbookdb.model.ContactType
import com.example.contactbookdb.model.Person
import com.example.contactbookdb.adapter.RecyclerViewPersonsAdapter
import com.example.contactbookdb.dialog.DialogContactAdd
import com.example.contactbookdb.dialog.DialogContactTypeAdd
import com.example.contactbookdb.dialog.DialogPersonAdd

class MainActivity : AppCompatActivity(), RecyclerViewPersonsAdapter.IPersonClickListener,
    DialogPersonEdit.IDialogPersonEditListener, RecyclerViewContactsAdapter.IContactClickListener, DialogPersonAdd.IDialogPersonAddListener, DialogContactAdd.IDialogContactAddListener {
    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecyclerViewPersonsAdapter
    private lateinit var repoHelper: RepoHelper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view_persons)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        resetData()
    }

    override fun onResume() {
        super.onResume()
        repoHelper = RepoHelper(this)
        adapter = RecyclerViewPersonsAdapter(this, repoHelper.persons)
        adapter.setPersonOnClickListener(this)
        adapter.setContactOnClickListener(this)
        recyclerView.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        repoHelper.closeAllRepositories()
    }

    private fun resetData() {
        val dbHelper =DbHelper(this)
        dbHelper.onUpgrade(dbHelper.writableDatabase, 1, 2)
        dbHelper.close()

        val repoHelper = RepoHelper(this)

        val p1 = repoHelper.addPerson(Person("Lilly", "Tobar"))
        val p2 = repoHelper.addPerson(Person("Pete", "Mohr"))
        val p3 = repoHelper.addPerson(Person("Mark", "Jonson"))
        val p4 = repoHelper.addPerson(Person("Joe", "Carter"))
        val ct1 = repoHelper.addContactType(ContactType("Skype"))
        val ct2 = repoHelper.addContactType(ContactType("E-mail"))
        val ct3 = repoHelper.addContactType(ContactType("Phone"))
        val c1 = repoHelper.addContact(Contact(p1, ct1, "lilly-tobar"))
        val c2 = repoHelper.addContact(Contact(p1, ct2, "lilly.tobar@yahoo.com"))
        val c3 = repoHelper.addContact(Contact(p1, ct3, "86957289"))
        val c4 = repoHelper.addContact(Contact(p2, ct1, "pete-mohr"))
        val c5 = repoHelper.addContact(Contact(p2, ct2, "pete.jonson@gmail.com", ))
        val c6 = repoHelper.addContact(Contact(p2, ct3, "29612042"))
        val c7 = repoHelper.addContact(Contact(p3, ct1, "mark-jonson"))
        val c8 = repoHelper.addContact(Contact(p3, ct2, "mark.jonson@gmail.com"))
        val c9 = repoHelper.addContact(Contact(p3, ct3, "572749273"))
        val c10 = repoHelper.addContact(Contact(p4, ct1, "joe-carter"))
        val c11 = repoHelper.addContact(Contact(p4, ct2, "joe.carter@icloud.com"))
        val c12 = repoHelper.addContact(Contact(p4, ct3, "835275643"))

        repoHelper.closeAllRepositories()
    }

    fun changeActivityOnClick(view: View) {
        val intent = Intent(this, ContactTypesActivity::class.java)
        startActivity(intent)

    }



    override fun onDialogPositiveClick(
        dialog: DialogFragment,
        id: Long,
        firstName: String,
        lastName: String
    ) {
        val person = repoHelper.persons.find { p -> p.id == id } ?: return
        person.firstName = firstName
        person.lastName = lastName
        repoHelper.updatePerson(person)
    }

    override fun onDeleteClicked(person: Person) {
        repoHelper.deletePerson(person)
    }

    override fun onEditClicked(person: Person) {
        Log.d(TAG, "onEditClicked")
        val dialog = DialogPersonEdit.newInstance(person.id, person.firstName, person.lastName)
        dialog.show(supportFragmentManager, "${this::class.java.simpleName}.${DialogPersonEdit::class.java.simpleName}")
    }

    override fun onAddContactClicked(person: Person) {
        val dialog = DialogContactAdd.newInstance(person.id, repoHelper.contactTypes.map { ct -> ct.id }.toLongArray(), repoHelper.contactTypes.map { ct -> ct.contactTypeValue }.toTypedArray())
        dialog.show(supportFragmentManager, "${this::class.java.simpleName}.${DialogContactAdd::class.java.simpleName}")
    }

    override fun onDeleteContactClicked(contact: Contact) {
        repoHelper.deleteContact(contact)
    }

    override fun onEditContactClicked(contact: Contact) {
        repoHelper.updateContact(contact)
    }

    fun recyclerViewPersonsFabAddOnClick(view: View) {
        val dialog = DialogPersonAdd()
        dialog.show(supportFragmentManager, "${this::class.java.simpleName}.${DialogPersonAdd::class.java.simpleName}")
    }

    override fun onDialogPositiveClick(
        dialog: DialogFragment,
        firstName: String,
        lastName: String
    ) {
        if (firstName.isNotBlank() && lastName.isNotBlank()) {
            repoHelper.addPerson(Person(firstName, lastName))
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, personId: Long, name: String, contactTypeId: Long) {
        if (name.isNotBlank()) {
            repoHelper.addContact(Contact(personId, contactTypeId, name))
            adapter.notifyDataSetChanged()
        }
    }

}