package com.example.contactbookdb

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_NAME = "contactDb.db"
        const val DATABASE_VERSION = 1

        const val PERSONS_TABLE_NAME = "PERSONS"

        const val PERSON_ID = "_id"
        const val PERSON_FIRSTNAME = "firstname"
        const val PERSON_LASTNAME = "lastname"

        const val SQL_PERSONS_CREATE_TABLE =
            "create table $PERSONS_TABLE_NAME (" +
                    "$PERSON_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$PERSON_FIRSTNAME TEXT NOT NULL, " +
                    "$PERSON_LASTNAME TEXT NOT NULL);"



        const val CONTACT_TYPES_TABLE_NAME = "CONTACT_TYPES"

        const val CONTACT_TYPE_ID = "_id"
        const val CONTACT_TYPE_VALUE = "contacttypevalue"

        const val SQL_CONTACT_TYPES_CREATE_TABLE =
            "create table $CONTACT_TYPES_TABLE_NAME (" +
                    "$CONTACT_TYPE_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$CONTACT_TYPE_VALUE TEXT NOT NULL);"


        const val CONTACTS_TABLE_NAME = "CONTACTS"

        const val CONTACT_ID = "_id"
        const val CONTACT_PERSON_ID = "contactpersonid"
        const val CONTACT_CONTACT_TYPE_ID = "contactcontacttypeid"
        const val CONTACT_CONTACT_NAME = "contactcontactname"

        const val SQL_CONTACTS_CREATE_TABLE =
            "create table $CONTACTS_TABLE_NAME (" +
                    "$CONTACT_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$CONTACT_PERSON_ID INTEGER NOT NULL, " +
                    "$CONTACT_CONTACT_TYPE_ID INTEGER NOT NULL, " +
                    "$CONTACT_CONTACT_NAME TEXT NOT NULL, " +
                    "FOREIGN KEY ($CONTACT_PERSON_ID) REFERENCES $PERSONS_TABLE_NAME($PERSON_ID) ON DELETE CASCADE, " +
                    "FOREIGN KEY ($CONTACT_CONTACT_TYPE_ID) REFERENCES $CONTACT_TYPES_TABLE_NAME($CONTACT_TYPE_ID) ON DELETE CASCADE);"

        const val SQL_DELETE_TABLE_PERSONS = "DROP TABLE IF EXISTS $PERSONS_TABLE_NAME;"
        const val SQL_DELETE_TABLE_CONTACTS = "DROP TABLE IF EXISTS $CONTACTS_TABLE_NAME;"
        const val SQL_DELETE_TABLE_CONTACT_TYPES = "DROP TABLE IF EXISTS $CONTACT_TYPES_TABLE_NAME;"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_DELETE_TABLE_CONTACT_TYPES)
        db?.execSQL(SQL_DELETE_TABLE_CONTACTS)
        db?.execSQL(SQL_DELETE_TABLE_PERSONS)
        db?.execSQL(SQL_PERSONS_CREATE_TABLE)
        db?.execSQL(SQL_CONTACT_TYPES_CREATE_TABLE)
        db?.execSQL(SQL_CONTACTS_CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_TABLE_CONTACT_TYPES)
        db?.execSQL(SQL_DELETE_TABLE_CONTACTS)
        db?.execSQL(SQL_DELETE_TABLE_PERSONS)
        onCreate(db)
    }
}