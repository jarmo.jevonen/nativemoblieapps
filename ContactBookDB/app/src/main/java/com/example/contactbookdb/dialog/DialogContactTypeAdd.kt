package com.example.contactbookdb.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import com.example.contactbookdb.R
import com.google.android.material.textfield.TextInputLayout
import java.lang.ClassCastException

class DialogContactTypeAdd : DialogFragment() {
    private lateinit var listener: IDialogContactTypeAddListener



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(this.context)
        val inflater = requireActivity().layoutInflater
        val inflatedView = inflater.inflate(R.layout.dialog_add_contact_type, null)
        builder.setView(inflatedView)
            .setPositiveButton("Add") { dialogInterface, i ->
                val contactTypeValue =
                    inflatedView.findViewById<TextInputLayout>(R.id.dialog_add_contact_type_text_input_layout_value).editText!!.text.toString()
                listener.onDialogPositiveClick(this, contactTypeValue)
                Log.d("Foo", "pos")
            }
            .setNegativeButton("Cancel") { dialogInterface, i ->
                Log.d("Foo", "neg")
                dialog?.cancel()
            }
            .setTitle(R.string.dialog_contact_type_add_title)
        return builder.create()
    }

    interface IDialogContactTypeAddListener {
        fun onDialogPositiveClick(dialog: DialogFragment, contactTypeValue: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IDialogContactTypeAddListener
        } catch (e: ClassCastException) {
            throw ClassCastException(("$context must implment ${this::class.java.simpleName}Listener"))
        }
    }
}