package com.example.contactbookdb.model

class ContactType {
    var id: Long = 0
    var contactTypeValue: String = ""
    var contacts: MutableList<Contact> = ArrayList()

    constructor(id : Long, contactTypeValue : String) {
        this.id = id
        this.contactTypeValue = contactTypeValue
    }

    constructor(contactTypeValue : String) : this(0L, contactTypeValue)
}