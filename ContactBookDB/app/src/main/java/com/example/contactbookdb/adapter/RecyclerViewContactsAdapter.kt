package com.example.contactbookdb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.contactbookdb.R
import com.example.contactbookdb.model.Contact
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.recycler_view_contacts_row.view.*

class RecyclerViewContactsAdapter(private val context: Context, private val contacts: MutableList<Contact>) : RecyclerView.Adapter<RecyclerViewContactsAdapter.ViewHolder>() {
    private val inflater = LayoutInflater.from(context)
    private lateinit var clickListener: IContactClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = inflater.inflate(R.layout.recycler_view_contacts_row, parent, false)
        return ViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts.get(position)
        holder.textInputLayoutName.editText!!.setText(contact.contactContactName)
        holder.textViewType.text = contact.contactType!!.contactTypeValue
        holder.buttonEdit.setOnClickListener {
            contact.contactContactName = holder.textInputLayoutName.editText!!.text.toString()
            clickListener.onEditContactClicked(contact)
            notifyDataSetChanged()
        }
        holder.buttonDelete.setOnClickListener {
            clickListener.onDeleteContactClicked(contact)
            notifyDataSetChanged()
        }
    }

    interface IContactClickListener {
        fun onDeleteContactClicked(contact: Contact)
        fun onEditContactClicked(contact: Contact)
    }

    fun setOnClickListener(clickListener: IContactClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return contacts.count()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textInputLayoutName: TextInputLayout = itemView.recycler_view_contacts_row_text_input_layout_name
        val textViewType: TextView = itemView.recycler_view_contacts_row_text_view_type
        val buttonEdit: Button = itemView.recycler_view_contacts_row_button_edit
        val buttonDelete: Button = itemView.recycler_view_contacts_row_button_delete
    }
}
