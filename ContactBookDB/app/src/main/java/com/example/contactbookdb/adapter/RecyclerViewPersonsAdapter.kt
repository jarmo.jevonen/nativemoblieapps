package com.example.contactbookdb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contactbookdb.R
import com.example.contactbookdb.dialog.DialogContactAdd
import com.example.contactbookdb.dialog.DialogPersonAdd
import com.example.contactbookdb.model.Contact
import com.example.contactbookdb.model.Person
import kotlinx.android.synthetic.main.recycler_view_persons_row.view.*

class RecyclerViewPersonsAdapter(private val context: Context, private val persons: MutableList<Person>) : RecyclerView.Adapter<RecyclerViewPersonsAdapter.ViewHolder>(),
    RecyclerViewContactsAdapter.IContactClickListener {

    private val inflater = LayoutInflater.from(context)
    private lateinit var clickPersonListener: IPersonClickListener
    private lateinit var clickContactListener : RecyclerViewContactsAdapter.IContactClickListener
    private lateinit var contactsAdapter: RecyclerViewContactsAdapter


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = inflater.inflate(R.layout.recycler_view_persons_row, parent, false)
        return ViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val person = persons[position]
        holder.textViewName.text = "${person.firstName} ${person.lastName}"
        holder.imageButtonEdit.setOnClickListener{
            clickPersonListener.onEditClicked(person)
            notifyDataSetChanged()
        }
        holder.imageButtonDelete.setOnClickListener {
            clickPersonListener.onDeleteClicked(person)
            notifyDataSetChanged()
        }
        holder.imageButtonAdd.setOnClickListener {
            clickPersonListener.onAddContactClicked(person)
        }
        holder.imageButtonExpand.setOnClickListener { view ->
            holder.imageButtonAdd.visibility = if (holder.imageButtonAdd.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            holder.recyclerViewContacts.visibility = if (holder.recyclerViewContacts.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            holder.textViewContactsLabel.visibility = if (holder.textViewContactsLabel.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            holder.imageButtonEdit.visibility = if (holder.imageButtonEdit.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            holder.imageButtonDelete.visibility = if (holder.imageButtonDelete.visibility == View.VISIBLE) View.GONE else View.VISIBLE

            holder.imageButtonExpand.setImageResource(if (holder.recyclerViewContacts.visibility == View.VISIBLE) R.drawable.ic_arrow_upward_24px else R.drawable.ic_arrow_downward_24px)
        }
        holder.recyclerViewContacts.layoutManager = LinearLayoutManager(context)
        holder.recyclerViewContacts.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        contactsAdapter = RecyclerViewContactsAdapter(context, person.contacts)
        contactsAdapter.setOnClickListener(this)
        holder.recyclerViewContacts.adapter = contactsAdapter
    }


    override fun getItemCount(): Int {
        return persons.count()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName: TextView = itemView.recycler_view_persons_row_textview_name
        val imageButtonEdit: ImageButton = itemView.recycler_view_persons_row_image_button_edit
        val imageButtonDelete: ImageButton = itemView.recycler_view_persons_row_image_button_delete
        val recyclerViewContacts: RecyclerView = itemView.recycler_view_persons_row_recycler_view_contacts

        val imageButtonExpand: ImageButton = itemView.recycler_view_persons_row_image_button_expand
        val imageButtonAdd: ImageButton = itemView.recycler_view_persons_row_image_button_add
        val textViewContactsLabel: TextView = itemView.recycler_view_persons_row_text_view_contacts_label
    }

    interface IPersonClickListener {
        fun onDeleteClicked(person: Person)
        fun onEditClicked(person: Person)
        fun onAddContactClicked(person: Person)
    }

    fun setPersonOnClickListener(clickListener: IPersonClickListener) {
        this.clickPersonListener = clickListener
    }

    fun setContactOnClickListener(contactClickListener: RecyclerViewContactsAdapter.IContactClickListener) {
        this.clickContactListener = contactClickListener
    }

    override fun onDeleteContactClicked(contact: Contact) {
        clickContactListener.onDeleteContactClicked(contact)
    }

    override fun onEditContactClicked(contact: Contact) {
        clickContactListener.onEditContactClicked(contact)
    }
}