package com.example.contactbookdb.model

class Contact {
    var id: Long = 0
    var contactPersonId: Long = 0
    var contactContactTypeId: Long = 0
    var contactContactName: String = ""
    var person: Person? = null
    var contactType: ContactType? = null


    constructor(id : Long, contactPersonId : Long, contactContactTypeId: Long, contactContactName: String) {
        this.id = id
        this.contactPersonId = contactPersonId
        this.contactContactTypeId = contactContactTypeId
        this.contactContactName = contactContactName
    }

    constructor(contactPersonId : Long, contactContactTypeId: Long, contactContactName: String) : this(0L, contactPersonId, contactContactTypeId, contactContactName)
}