package com.example.contactbookdb.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import com.example.contactbookdb.R
import com.google.android.material.textfield.TextInputLayout
import java.lang.ClassCastException

class DialogPersonEdit : DialogFragment() {
    companion object {
        const val PERSON_ID = "DialogPersonEditPersonID"
        const val PERSON_OLD_FIRST_NAME = "DialogPersonEditOldFirstName"
        const val PERSON_OLD_LAST_NAME = "DialogPersonEditOldLastName"

        fun newInstance(personId: Long, personOldFirstName: String, personOldLastName: String) : DialogPersonEdit {
            val args = Bundle()
            args.putLong(PERSON_ID, personId)
            args.putString(PERSON_OLD_FIRST_NAME, personOldFirstName)
            args.putString(PERSON_OLD_LAST_NAME, personOldLastName)

            val dialog = DialogPersonEdit()
            dialog.arguments = args
            return dialog
        }
    }


    internal lateinit var listener: IDialogPersonEditListener





    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(this.context)
        val inflater = requireActivity().layoutInflater
        val inflatedView = inflater.inflate(R.layout.dialog_edit_person, null)

        val textInputLayoutFirstName = inflatedView.findViewById<TextInputLayout>(R.id.dialog_edit_person_text_input_layout_first_name)
        val textInputLayoutLastName = inflatedView.findViewById<TextInputLayout>(R.id.dialog_edit_person_text_input_layout_last_name)

        val id = arguments?.getLong(PERSON_ID, 0L)
        val oldFirstName = arguments?.getString(PERSON_OLD_FIRST_NAME, "")
        val oldLastName = arguments?.getString(PERSON_OLD_LAST_NAME, "")
        if (id == null || id == 0L) {
            dismiss()
        }



        textInputLayoutFirstName.editText!!.setText(oldFirstName)
        textInputLayoutLastName.editText!!.setText(oldLastName)


        builder.setView(inflatedView)
            .setPositiveButton("Save") { dialogInterface, i ->
                val firstName =
                    textInputLayoutFirstName.editText!!.text.toString()
                val lastName =
                    textInputLayoutLastName.editText!!.text.toString()
                listener.onDialogPositiveClick(this, id!!, firstName, lastName)
                Log.d("Foo", "pos")
            }
            .setNegativeButton("Cancel") { dialogInterface, i ->
                Log.d("Foo", "neg")
                dialog?.cancel()
            }
            .setTitle(R.string.dialog_person_edit_title)
        return builder.create()
    }

    interface IDialogPersonEditListener {
        fun onDialogPositiveClick(dialog: DialogFragment, id:Long, firstName: String, lastName: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as IDialogPersonEditListener
        } catch (e: ClassCastException) {
            throw ClassCastException(("$context must implment ${this::class.java.simpleName}Listener"))
        }
    }
}