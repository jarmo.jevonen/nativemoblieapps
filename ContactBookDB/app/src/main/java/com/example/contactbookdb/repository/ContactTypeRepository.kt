package com.example.contactbookdb.repository


import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.contactbookdb.DbHelper
import com.example.contactbookdb.model.ContactType


class ContactTypeRepository(val context: Context) {
    private lateinit var dbHelper: DbHelper
    private lateinit var db: SQLiteDatabase

    fun open(): ContactTypeRepository {
        dbHelper = DbHelper(context)
        db = dbHelper.writableDatabase
        db.execSQL("PRAGMA foreign_keys=ON")
        return this
    }

    fun close() {
        dbHelper.close()
    }

    fun delete(contactType: ContactType) {
        db.delete(DbHelper.CONTACT_TYPES_TABLE_NAME, "${DbHelper.CONTACT_TYPE_ID} = ?", arrayOf(contactType.id.toString()))
    }

    fun add(contactType : ContactType): Long {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.CONTACT_TYPE_VALUE, contactType.contactTypeValue)
        contactType.id = db.insert(DbHelper.CONTACT_TYPES_TABLE_NAME, null, contentValues)
        return contactType.id
    }

    fun update(contactType: ContactType) {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.CONTACT_TYPE_ID, contactType.id)
        contentValues.put(DbHelper.CONTACT_TYPE_VALUE, contactType.contactTypeValue)
        db.update(DbHelper.CONTACT_TYPES_TABLE_NAME, contentValues, "${DbHelper.CONTACT_TYPE_ID} = ?", arrayOf(contactType.id.toString()))
    }

    fun getAll(): MutableList<ContactType> {
        val contactTypes = ArrayList<ContactType>()

        val columns = arrayOf(DbHelper.CONTACT_TYPE_ID, DbHelper.CONTACT_TYPE_VALUE)

        val cursor = db.query(DbHelper.CONTACT_TYPES_TABLE_NAME, columns, null, null, null, null, null)

        while (cursor.moveToNext()) {
            contactTypes.add(
                ContactType(
                    cursor.getLong(cursor.getColumnIndex(DbHelper.CONTACT_TYPE_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.CONTACT_TYPE_VALUE))
                )
            )
        }
        cursor.close()
        return contactTypes
    }
}
