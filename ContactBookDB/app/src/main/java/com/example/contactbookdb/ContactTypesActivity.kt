package com.example.contactbookdb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contactbookdb.dialog.DialogContactTypeAdd
import com.example.contactbookdb.model.ContactType
import com.example.contactbookdb.adapter.RecyclerViewContactTypesAdapter

class ContactTypesActivity : AppCompatActivity(), RecyclerViewContactTypesAdapter.IContactTypeClickListener, DialogContactTypeAdd.IDialogContactTypeAddListener{
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecyclerViewContactTypesAdapter
    private lateinit var repoHelper: RepoHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_types)
        recyclerView = findViewById(R.id.recycler_view_contact_types)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    override fun onResume() {
        super.onResume()
        repoHelper = RepoHelper(this)
        adapter = RecyclerViewContactTypesAdapter(this, repoHelper.contactTypes)
        adapter.setOnClickListener(this)
        recyclerView.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        repoHelper.closeAllRepositories()
    }

    override fun onDeleteClicked(contactType: ContactType) {
        repoHelper.deleteContactType(contactType)
        adapter.notifyDataSetChanged()
    }

    override fun onEditClicked(contactType: ContactType) {
        repoHelper.updateContactType(contactType)
        adapter.notifyDataSetChanged()
    }

    fun recyclerViewContactTypesFabAddOnClick(view: View) {
        val dialog = DialogContactTypeAdd()
        dialog.show(supportFragmentManager, "${this::class.java.simpleName}.${DialogContactTypeAdd::class.java.simpleName}")
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, contactTypeValue: String) {
        if (contactTypeValue.isNotBlank()) {
            repoHelper.addContactType(ContactType(contactTypeValue))
            adapter.notifyDataSetChanged()
        }
    }


}