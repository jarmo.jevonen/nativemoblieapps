package com.example.contactbookdb.repository


import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.contactbookdb.DbHelper
import com.example.contactbookdb.model.Person


class PersonRepository(val context: Context) {
    private lateinit var dbHelper: DbHelper
    private lateinit var db: SQLiteDatabase

    fun open(): PersonRepository {
        dbHelper = DbHelper(context)
        db = dbHelper.writableDatabase
        db.execSQL("PRAGMA foreign_keys=ON")
        return this
    }

    fun close() {
        dbHelper.close()
    }

    fun update(person: Person) {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.PERSON_ID, person.id)
        contentValues.put(DbHelper.PERSON_FIRSTNAME, person.firstName)
        contentValues.put(DbHelper.PERSON_LASTNAME, person.lastName)
        db.update(DbHelper.PERSONS_TABLE_NAME, contentValues, "${DbHelper.PERSON_ID} = ?", arrayOf(person.id.toString()))
    }

    fun delete(person: Person) {
        db.delete(DbHelper.PERSONS_TABLE_NAME, "${DbHelper.PERSON_ID} = ?", arrayOf(person.id.toString()))
    }

    fun add(person : Person): Long {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.PERSON_FIRSTNAME, person.firstName)
        contentValues.put(DbHelper.PERSON_LASTNAME, person.lastName)
        person.id = db.insert(DbHelper.PERSONS_TABLE_NAME, null, contentValues)
        return person.id
    }

    fun getAll(): MutableList<Person> {
        val persons = ArrayList<Person>()

        val columns = arrayOf(
            DbHelper.PERSON_ID,
            DbHelper.PERSON_FIRSTNAME,
            DbHelper.PERSON_LASTNAME
        )

        val cursor = db.query(DbHelper.PERSONS_TABLE_NAME, columns, null, null, null, null, null)

        while (cursor.moveToNext()) {
            persons.add(
                Person(
                    cursor.getLong(cursor.getColumnIndex(DbHelper.PERSON_ID)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.PERSON_FIRSTNAME)),
                    cursor.getString(cursor.getColumnIndex(DbHelper.PERSON_LASTNAME))
                )
            )
        }
        cursor.close()
        return persons
    }
}
