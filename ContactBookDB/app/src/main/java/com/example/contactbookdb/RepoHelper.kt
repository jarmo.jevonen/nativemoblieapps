package com.example.contactbookdb

import android.content.Context
import com.example.contactbookdb.model.Contact
import com.example.contactbookdb.model.ContactType
import com.example.contactbookdb.model.Person
import com.example.contactbookdb.repository.ContactRepository
import com.example.contactbookdb.repository.ContactTypeRepository
import com.example.contactbookdb.repository.PersonRepository

class RepoHelper(val context: Context) {
    private var personRepository: PersonRepository
    private var contactTypeRepository: ContactTypeRepository
    private var contactRepository: ContactRepository
    var persons: MutableList<Person>
    var contactTypes: MutableList<ContactType>
    var contacts: MutableList<Contact>


    init {
        personRepository = PersonRepository(context).open()
        contactTypeRepository = ContactTypeRepository(context).open()
        contactRepository = ContactRepository(context).open()
        persons = personRepository.getAll()
        contactTypes = contactTypeRepository.getAll()
        contacts = contactRepository.getAll()

        assignValuesFromAllRepositories()
    }


    fun closeAllRepositories() {
        personRepository.close()
        contactTypeRepository.close()
        contactRepository.close()
    }


    fun assignValuesFromAllRepositories() {
        for (contact in contacts) {
            val contactType = contactTypes.find{ct -> ct.id == contact.contactContactTypeId}
            val person = persons.find { p -> p.id == contact.contactPersonId }
            contact.contactType = contactType
            contactType!!.contacts.add(contact)
            contact.person = person
            person!!.contacts.add(contact)
        }
    }

    fun addPerson(person: Person): Long {
        person.id = personRepository.add(person)
        persons.add(person)
        return person.id
    }

    fun addContactType(contactType: ContactType): Long {
        contactType.id = contactTypeRepository.add(contactType)
        contactTypes.add(contactType)
        return contactType.id
    }

    fun addContact(contact: Contact): Long {
        contact.id = contactRepository.add(contact)
        contact.person = persons.find { person -> person.id == contact.contactPersonId }
        contact.contactType = contactTypes.find { contactType -> contactType.id == contact.contactContactTypeId }
        contacts.add(contact)
        contact.person!!.contacts.add(contact)
        contact.contactType!!.contacts.add(contact)
        return contact.id
    }

    fun deletePerson(person: Person) {
        for (contact in person.contacts) {
            contact.contactType?.contacts?.remove(contact)
            contactRepository.delete(contact)
            contacts.remove(contact)
        }
        personRepository.delete(person)
        persons.remove(person)
    }

    fun deleteContactType(contactType: ContactType) {
        for (contact in contactType.contacts) {
            contact.person?.contacts?.remove(contact)
            contactRepository.delete(contact)
            contacts.remove(contact)
        }
        contactTypeRepository.delete(contactType)
        contactTypes.remove(contactType)
    }

    fun deleteContact(contact: Contact) {
        contact.contactType?.contacts?.remove(contact)
        contact.person?.contacts?.remove(contact)
        contactRepository.delete(contact)
        contacts.remove(contact)
    }

    fun updatePerson(person: Person) {
        personRepository.update(person)
    }

    fun updateContactType(contactType: ContactType) {
        contactTypeRepository.update(contactType)
    }

    fun updateContact(contact: Contact){
        contactRepository.update(contact)
    }
}