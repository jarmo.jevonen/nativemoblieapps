//
//  EPlayer.swift
//  Connect4
//
//  Created by Jarmo Jevonen on 24.11.2020.
//

import Foundation

enum EPlayer {
    case Empty
    case Player1
    case Player2
}
