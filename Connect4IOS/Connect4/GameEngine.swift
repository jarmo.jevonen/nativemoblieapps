//
//  GameEngine.swift
//  Connect4
//
//  Created by Jarmo Jevonen on 23.11.2020.
//

import Foundation

class GameEngine {
    
    struct Cell {
        var player: EPlayer
        var winningComb: Bool
    }
    
    
    
    init(controller: ViewController) {
        gameBoard = [[Cell]]()
        self.controller = controller
        initializeGameBoard()
    }
    
    private var isMoveByPlayer1 = true
    private var gameOver = false
    private var controller: ViewController
    
    private var gameBoard: [[Cell]]
    
    func resetGame() {
        gameBoard = [[Cell]]()
        initializeGameBoard()
        gameOver = false
        isMoveByPlayer1 = true
    }
    
    func makeAMove(_ y: Int,_ x: Int) {
        var lastX = -1
        var lastY = -1
        if !gameOver {
            for y in stride(from: 5, to: -1, by: -1) {
                if gameBoard[y][x].player == EPlayer.Empty {
                    gameBoard[y][x].player = isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2
                    controller.showPlayerMove(y: y, x: x, player: gameBoard[y][x].player)
                    lastX = x
                    lastY = y
                    break
                }
            }
            hasPlayerWon(y: lastY, x: lastX)
            isDraw()
            if (!gameOver) {
                isMoveByPlayer1 = !isMoveByPlayer1
                controller.showDataOnPlayerTurnLabel(player: isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2, won: false)
            }
        }
    }
    
    private func hasPlayerWon(y: Int, x: Int) {
        wonVertically(xCo: x)
        wonHorizontally(yCo: y)
        wonDiagonalUp(yCoordinate: y, xCoordinate: x)
        wonDiagonalDown(yCoordinate: y, xCoordinate: x)
    }
    
    private func isDraw() {
        var hasEmptyCells = false
        for y in 0...5 {
            for x in 0...6 {
                if gameBoard[y][x].player == EPlayer.Empty {
                    hasEmptyCells = true
                    break
                }
            }
        }
        if !hasEmptyCells {
            gameOver = true
            controller.declareDraw()
        }
    }

    
    private func wonVertically(xCo: Int) {
        let currentPlayer = isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2
        var winningCombination = [Cell]()
        for y in stride(from: 5, to: -1, by: -1) {
            if gameBoard[y][xCo].player == currentPlayer {
                winningCombination.append(gameBoard[y][xCo])
            } else if gameBoard[y][xCo].player == EPlayer.Empty {
                continue
            } else {
                winningCombination = [Cell]()
            }
        }
        let b = checkForWinAndMarkCells(combination: winningCombination)
        if b {
            print("VER:")
        }
    }
    
    
    private func wonHorizontally(yCo: Int) {
        var highestWinCombination = [Cell]()
        var currentCombination = [Cell]()
        let currentPlayer = isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2
        for x in 0...6 {
            if gameBoard[yCo][x].player == currentPlayer {
                currentCombination.append(gameBoard[yCo][x])
            } else {
                highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
                currentCombination.removeAll()
            }
        }
        highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
        let b = checkForWinAndMarkCells(combination: highestWinCombination)
        if b {
            print("HOR:")
        }
    }
    
    private func wonDiagonalUp(yCoordinate: Int, xCoordinate: Int) {
        let currentPlayer = isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2
        let startingPoint = calculateDiagonallyUpStartingPoint(y: yCoordinate, x: xCoordinate)
        var highestWinCombination = [Cell]()
        var currentCombination = [Cell]()
        var xCo = startingPoint.x
        for y in startingPoint.y...5 {
            if (xCo < 0 || y < 0) {
                break
            }
            if (gameBoard[y][xCo].player == currentPlayer) {
                currentCombination.append(gameBoard[y][xCo])
            } else {
                highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
                currentCombination.removeAll()
            }
            xCo = xCo - 1
        }
        highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
        let b = checkForWinAndMarkCells(combination: highestWinCombination)
        if b {
            print("diaUP:")
        }
    }
    
    private func wonDiagonalDown(yCoordinate: Int, xCoordinate: Int) {
        let currentPlayer = isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2
        let startingPoint = calculateDiagonallyDownStartingPoint(y: yCoordinate, x: xCoordinate)
        var highestWinCombination = [Cell]()
        var currentCombination = [Cell]()
        var xCo = startingPoint.x
        for y in startingPoint.y...5 {
            if (xCo > 6) {
                break
            }
            if (gameBoard[y][xCo].player == currentPlayer) {
                currentCombination.append(gameBoard[y][xCo])
            } else {
                highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
                currentCombination.removeAll()
            }
            xCo = xCo + 1
        }
        highestWinCombination = getHigherCountArray(highestWinCombination, currentCombination)
        let b = checkForWinAndMarkCells(combination: highestWinCombination)
        if b {
            print("diaDOWN:")
        }
    }

    
    
    private func getHigherCountArray(_ first: [Cell], _ second: [Cell]) -> [Cell] {
        if first.count > second.count {
            return first
        }
        return second
    }
    
    private func calculateDiagonallyUpStartingPoint(y: Int, x: Int) -> (y: Int, x: Int) {
        
        var yCoordinate = 0
        var xCoordinate = y + x
        if xCoordinate > 6 {
            yCoordinate = xCoordinate - 6
            xCoordinate = 6
        }
        
        return (yCoordinate, xCoordinate)
    }
    
    private func calculateDiagonallyDownStartingPoint(y: Int, x: Int) -> (y: Int, x: Int) {
        print("given: \(y) \(x)")
        var yCoordinate = y - x
        var xCoordinate = 0
        if yCoordinate < 0 {
            xCoordinate = abs(yCoordinate)
            yCoordinate = 0
        }
        print(yCoordinate, xCoordinate)
        return (yCoordinate, xCoordinate)
    }

    
    
    private func checkForWinAndMarkCells(combination: [Cell]) -> Bool {
        if combination.count >= 4 {
            for var cell in combination {
                cell.winningComb = true
            }
            gameOver = true
            controller.showDataOnPlayerTurnLabel(player: isMoveByPlayer1 ? EPlayer.Player1 : EPlayer.Player2, won: true)
            return true
        }
        return false
    }
    
    private func initializeGameBoard() {
        for _ in 1...6 {
            var row = [Cell]()
            for _ in 1...7 {
                let cell = Cell(player: EPlayer.Empty, winningComb: false)
                row.append(cell)
            }
            gameBoard.append(row)
        }
    }
}
