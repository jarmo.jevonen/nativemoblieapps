//
//  ViewController.swift
//  Connect4
//
//  Created by Jarmo Jevonen on 23.11.2020.
//

import UIKit

class ViewController: UIViewController {

    var gameEngine: GameEngine?
    var buttons : [UIButton] = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameEngine = GameEngine(controller: self)
        CreateBoard()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for button in buttons {
            button.clipsToBounds = true
            button.layer.cornerRadius = 0.5 * button.bounds.size.width
        }
        
    }
    
    
    @IBOutlet weak var nextPlayerLabel: UILabel!
    
    @IBAction func startNewGameOnClick() {
        if let game = gameEngine {
            game.resetGame()
            for button in buttons {
                button.backgroundColor = #colorLiteral(red: 0.8593991059, green: 0.9259836464, blue: 0.804362132, alpha: 1)
            }
        }
    }
    @IBOutlet weak var gameBoardStackView: UIStackView!
    
    @objc func buttonClicked(_ sender: UIButton) {
        let tileId = sender.accessibilityLabel
        let y = Int(tileId!.prefix(1))
        let x = Int(tileId!.suffix(1))
        
        print("\(String(describing: sender.accessibilityLabel))")
        if let yCo = y, let xCo = x, let game = gameEngine {
            game.makeAMove(yCo, xCo)
        }
    }
    
    func CreateBoard() {
        for y in 0...5 {
            let row = UIStackView()
            row.axis = .horizontal
            row.alignment = .fill
            row.distribution = .fillEqually
            row.spacing = 8.0
            for x in 0...6 {
                let button = UIButton()
                let buttonId = String(format: "%x%x", y, x)
                button.accessibilityLabel = buttonId
                button.backgroundColor = #colorLiteral(red: 0.8593991059, green: 0.9259836464, blue: 0.804362132, alpha: 1)
                button.addTarget(self, action: #selector(buttonClicked(_:)), for: UIControl.Event.touchUpInside)
                row.addArrangedSubview(button)
                buttons.append(button)
            }
            gameBoardStackView.addArrangedSubview(row)
        }
    }
    
    func showDataOnPlayerTurnLabel(player: EPlayer, won: Bool) {
        let playerStr = player == EPlayer.Player1 ? "Blue" : "Red"
        var infoStr = "Next turn:"
        if won {
            infoStr = "Winner is"
        }
        nextPlayerLabel.text = "\(infoStr) \(playerStr)"
    }
    
    func showPlayerMove(y: Int, x: Int, player: EPlayer) {
        let stackView = gameBoardStackView.arrangedSubviews[y] as? UIStackView
        var color = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if let row = stackView {
            let button = row.arrangedSubviews[x] as? UIButton
            if let tile = button {
                switch player {
                case EPlayer.Player1:
                    color = #colorLiteral(red: 0, green: 0.8797622993, blue: 1, alpha: 1)
                case EPlayer.Player2:
                    color = #colorLiteral(red: 1, green: 0.06819190208, blue: 0.563583147, alpha: 1)
                default:
                    color = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }
                tile.backgroundColor = color
            }
        }
    }
    
    func declareDraw() {
        nextPlayerLabel.text = "Draw!"
    }
}
